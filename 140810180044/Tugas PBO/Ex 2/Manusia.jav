public class Manusia {
    private int npm;
    private String nama;

    public Manusia(){
        this.nama = "";
    }

    public void setnpm(int npm) {
        this.npm = npm;
    }

    public int getnpm() {
        return this.npm;
    }

    public void setnama(String nama ) {
        this.nama = nama;
    }

    public String getnama() {
        return this.nama;
    }
}