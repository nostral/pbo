/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
import java.util.Scanner;

public class Test {
    public static void main(String args[]) {
        Scanner  sc = new Scanner(System.in);
        Manusia mhs = new Manusia();

        System.out.println("Masukan 6 digit terakhir dari NPM : "); 
        int i = Integer.parseInt(sc.nextLine());
        
        mhs.setnpm(i);

        System.out.println("Nama : "); 
        String s = sc.nextLine();
        
        mhs.setnama(s);

        System.out.println("Nama saya " + mhs.getnama() + " dengan NPM " + mhs.getnpm());
    }
}