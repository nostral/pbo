/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/

abstract class AbstractMovablePoint{
    private int x;
    private int y;

    public AbstractMovablePoint(int x, int y){
        this.x=x;
        this.y=y;
    }

    public String toString() {
        return "MovablePoint{" + this.x + this.y + '}';
    }

    public void moveLeft(){
        this.y--;
    }

    
    public void moveRight(){
        this.y++;
    }
    
    public void moveUp(){
        this.x++;
    }

    
    public void moveDown(){
        this.x--;
    }
}


class MovablePoint extends AbstractMovablePoint{
    public MovablePoint(int x, int y){
        super(x, y);
    }
} 


public class Test {
    public static void main(String[] args){
        MovablePoint point1 = new MovablePoint(0,0);
        point1.moveUp();
        point1.moveUp();
        point1.moveDown();
        point1.moveRight();
        point1.moveRight();
        point1.moveRight();
        point1.moveLeft();
        System.out.println(point1);
    }
}



