/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/

interface Movable{
    public void moveUp();
    public void moveDown();
    public void moveLeft();
    public void moveRight();
}

public class MovablePoint implements Movable{
    private int x;
    private int y;

    public MovablePoint(int x, int y){
        this.x=x;
        this.y=y;
    }

    @Override
    public String toString() {
        return "MovablePoint{" + this.x + this.y + '}';
    }

    @Override
    public void moveUp(){
        this.x++;
    }

    @Override
    public void moveDown(){
        this.x--;
    }

    @Override
    public void moveLeft(){
        this.y--;
    }

    @Override
    public void moveRight(){
        this.y++;
    }

    public static void main(String[] args){
        MovablePoint point1 = new MovablePoint(0,0);
        point1.moveUp();
        point1.moveUp();
        point1.moveDown();
        point1.moveRight();
        point1.moveRight();
        point1.moveRight();
        point1.moveLeft();
        System.out.println(point1);
    
        Movable point2 = new MovablePoint(3,2);
        Test.printMovablePoint((MovablePoint)point2);
    }
} 


class Test {
    public static void printMovablePoint(MovablePoint point){
        System.out.println(point);
    }
}



