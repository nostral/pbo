/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
import java.util.Scanner;

public class 1{
    public static void main(String[] args){
        int menu;
        int x;
        Integer y;
        String z;
        Scanner input = new Scanner(System.in);
        Scanner sc = new Scanner(System.in);
        System.out.println("Menu Konversi");
        System.out.println("1. Input int Output Integer");
        System.out.println("2. Input int Output String");
        System.out.println("3. Input Integer Output int");
        System.out.println("4. Input Integer Output String");
        System.out.println("5. Input String Output int");
        System.out.println("6. Input String Output Integer");
        System.out.print("Pilih : ");
        menu = input.nextInt();
        switch(menu){
            case 1:
            System.out.print("Masukan angka : ");
            x = input.nextInt();
            y = Integer.valueOf(x);
            System.out.print("Hasil konversi : "+y);
            System.out.print(" ("+y.getClass().getSimpleName()+")");
            break;
            case 2:
            System.out.print("Masukan angka : ");
            x = input.nextInt();
            z = Integer.toString(x);
            System.out.print("Hasil konversi : "+z);
            System.out.print(" ("+z.getClass().getSimpleName()+")");
            break;
            case 3:
            System.out.print("Masukan angka : ");
            y = input.nextInt();
            x = y.intValue();
            System.out.print("Hasil konversi : "+x);
            break;
            case 4:
            System.out.print("Masukan angka : ");
            y = input.nextInt();
            z = y.toString();
            System.out.print("Hasil konversi : "+z);
            System.out.print(" ("+z.getClass().getSimpleName()+")");
            break;
            case 5:
            System.out.print("Masukan angka : ");
            z = sc.nextLine();
            x = Integer.parseInt(z);
            System.out.print("Hasil konversi : "+x);
            break;
            case 6:
            System.out.print("Masukan angka : ");
            z = sc.nextLine();
            y = Integer.valueOf(z);
            System.out.print("Hasil konversi : "+y);
            System.out.print(" ("+y.getClass().getSimpleName()+")");
            break;
        }
    }
}