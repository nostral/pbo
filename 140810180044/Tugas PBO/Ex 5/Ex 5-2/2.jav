/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/

enum Buah{
    MANGGA(10000), APEL(11000), MELON(12000), JERUK(5000), ANGGUR(15000);
    private int harga;                

    Buah(int harga) {                   
        this.harga = harga; 
    }

    public int getHarga() {         
        return harga;
    }

    public String getDeskripsi() {    
        switch(this) {
            case MANGGA:
            return "Silakan";

            case APEL:
            return "MePhone";

            case ANGGUR:
            return "Grombolan";

            case JERUK:
            return "Buah berwarna oranye";

            case MELON:
            return "Doto";

            default:
            return "Error";
        }
    }
}

public class soal02{
    public static void main(String[] args) {
        for(Buah buah : Buah.values()) {
            System.out.println(buah +"\nHarga Buah : " + buah.getHarga() + "\nDeskripsi : " + buah.getDeskripsi());
        }  
    }
}