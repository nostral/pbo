/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
import java.util.Scanner;

public class 4{
    public static int num1;
    public static int num2;
    public static float num3;
    public static float num4;

    public static int cnt(int num1, int num2){
        return num1 + num2;
    }

    public static float cnt(float num1, float num2){
        return num1 + num2;
    }

    public static void main (String args[]){
        Scanner sc = new Scanner(System.in);

        System.out.println("Angka ke-1: ");
        num1 = sc.nextInt();

        System.out.println("Angka ke-2: ");
        num2 = sc.nextInt();

        System.out.println("Angka ke-3: ");
        num3 = sc.nextFloat();

        System.out.println("Angka ke-4: ");
        num4 = sc.nextFloat();
        
        System.out.println("Hasil tambah: " + cnt(num1,num2));
        System.out.println("Hasil tambah: " + cnt(num3, num4));
    }
}