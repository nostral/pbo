/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
package anak1;

import java.util.Scanner;
import anak2.util;

public class dt{
    public static void main (String args[]){
        Scanner sc = new Scanner(System.in);

        System.out.println("Angka ke-1: ");
        int num1 = sc.nextInt();
        
        System.out.println("Angka ke-2: ");
        int num2 = sc.nextInt();
        
        System.out.println("Operasi yang diinginkan: (+|-|x|/)");
        char ops = sc.next().charAt(0);
        
        util utility = new util(num1, num2, ops);
    }
}