/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
public class Manusia {
    private int npm;
    private String nama;

    public Manusia(String nama, int npm){
        this.nama = nama;
        this.npm = npm;
    }

    public void setnpm(int npm) {
        this.npm = npm;
    }

    public int getnpm() {
        return this.npm;
    }

    public void setnama(String nama ) {
        this.nama = nama;
    }

    public String getnama() {
        return this.nama;
    }
}