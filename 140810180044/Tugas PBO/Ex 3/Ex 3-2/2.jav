/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
import java.util.Scanner;

public class 2 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        Manusia manusia[] = new Manusia[5];
        for(int i = 0; i < 5; i++){
            System.out.println("Iterasi ke-" + (i + 1));
            
            System.out.println("Masukan Nama: ");
            String nama = sc.nextLine();

            System.out.println("NPM : ");
            int npm = Integer.parseInt(sc.nextLine());
            manusia[i] = new Manusia(nama, npm);
        }
        for(int i = 0; i < 5; i++){
            System.out.println("I ke-" + (i + 1));
            System.out.println("Nama: " + manusia[i].getnama());
            System.out.println("NPM : " + manusia[i].getnpm());
        }
    }
}
