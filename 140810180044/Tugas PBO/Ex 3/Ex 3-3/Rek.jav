/*	
Nama    : Fauzan Naufal T
Kelas	: A
NPM		: 180044
*/
import java.util.Scanner;

public class rekening{
    private int balance;
    
    public rekening(){
        this.balance = 0;
    }

    public void addBalance(int balance) {
        this.balance = this.balance + balance;
    }

    public int getBalance() {
        return this.balance;
    } 
}